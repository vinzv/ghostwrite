﻿## GhostWrite theme for Anchor

A theme for Anchor CMS, merged from Kostya Kozak's Write and inspired by Ghost's default theme "Casper".

![Responsive.](https://raw2.github.com/vinzv/ghostwrite/master/screenshot-anchor.jpg)

**Why GhostWrite?**

* Clean & minimal.
* Typography-direction.
* DiSQUS-powered comments.
* Responsive.

[Demo? Here.](https://anchor.vinzv.de)

Love it? Really? Thanks. Go to Twitter and [follow me](http://twitter.com/vinzv). You are beautiful man!

### How install

1. Download Write theme (from this repo).
2. Upload `write` to `/themes` folder (at your site).
3. Activate theme in `Extend` → `Site Metadata`
5. Boom. You lucky.

### Help & Support

I can help you [in Twitter](//twitter.com/vinzv).
