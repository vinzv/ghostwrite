<!doctype html>
<html>
  <head>
  
    <meta charset="utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    
    <title><?php echo page_title('Page can’t be found'); ?> - <?php echo site_name(); ?></title>
    
    <meta name="description" content="<?php echo site_description(); ?>">
    
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <!--[if lt IE 9]>
      <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    
    <link rel="stylesheet" href="<?php echo theme_url('/css/i.css'); ?>">
    
    <link rel="shortcut icon" href="<?php echo theme_url('/img/favicon.png'); ?>">
    
    <meta name="generator" content="Anchor CMS">

    <?php if(customised()): ?>
        <!-- Custom CSS -->
    		<style><?php echo article_css(); ?></style>

    	<!--  Custom Javascript -->
    		<script><?php echo article_js(); ?></script>
	<?php endif; ?>
    
  </head>
  
  <body>
  
    <header class="top" style="background-image:url(<?php echo theme_url('/img/header.jpg'); ?>);">
    
        <h1 class="h1 site-title site-logo mega center lh-1"><a href="/"><?php echo site_name(); ?></a>	</h1>

	<h2 class="h2-logo tagline site-logo center serif title-spacing">&raquo;&nbsp;<?php echo site_description(); ?>&nbsp;&laquo;</h2>
      
    </header>
